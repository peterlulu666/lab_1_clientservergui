# Yanzhi Wang
# 1001827416
# We will import the socket module
import socket

# We will import the tkinter module
import tkinter

# We will import the threading module
import threading
from _thread import *

# The code regarding the TCP socket programming is learned from the textbook Computer Networking A Top-Down Approach 2.7.2
# It is the same port number as in the client
server_port = 13000
# We will create the server’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# We will bind the port number 13000 to the server’s socket
server_socket.bind(("", server_port))
# The server_socket is the welcoming socket
# After establishing this welcoming door,
# we will wait and listen for some client to knock on the door
# The server will listen for TCP connection requests from the client
# The server should be able to handle all three clients simultaneously
# the argument is 3
server_socket.listen(3)

# We will use list to store the user info
user_list = []

# We will use list to store the connection_socket
connection_socket_list = []


# The server will check the duplicated user,
# receive file,
# process file, and
# send file to client
def user_check():
    """
    The server will check the duplicated user
    """
    global user_list
    while True:
        # When a client knocks on this door,
        # it will use the accept() to create a new socket in the server
        connection_socket, address = server_socket.accept()
        # The client and server will complete the handshaking and
        # create a TCP connection between the client’s client_socket and
        # the server’s connection_socket
        # If we have the TCP connection, we will receive data from client
        receive_info = connection_socket.recv(1024)
        # The received info is the user info and we will decode the data
        user_info = receive_info.decode()

        # We will check duplicated user
        if user_info not in user_list:
            send_info = "The user is not duplicated. You are connecting to the server. "
            # If the user is not duplicated, we will store it is the list
            user_list.append(user_info)
            # If the connection_socket is not duplicated, we will store it is the list
            connection_socket_list.append(connection_socket)
            # Indicate the user connecting info
            user_label.config(text=user_info + " is connecting to the server. ")
            # We will notify the client that if the user is duplicate
            # We will send the info through the server's socket and into the TCP connection
            encode_info_to_bytes = send_info.encode()
            connection_socket.send(encode_info_to_bytes)
            # The server should indicate which of those clients are presently connected on its GUI
            user_list_label.config(text="The user currently connecting to the server: " + str(user_list))
            # If the client’s username is available,
            # fork a thread to handle that client
            # We will create the thread
            start_new_thread(word_process, (connection_socket, user_info,))
        else:
            send_info = "The user is duplicated. Enter a different user. "
            # Indicate the user connecting info
            user_label.config(text=user_info + " is in the user list. It is duplicated. ")
            # We will notify the client that if the user is duplicate
            # We will send the info through the server's socket and into the TCP connection
            encode_info_to_bytes = send_info.encode()
            connection_socket.send(encode_info_to_bytes)
            # The server should indicate which of those clients are presently connected on its GUI
            user_list_label.config(text="The user currently connecting to the server: " + str(user_list))
            # If the user is duplicated, disconnect the connection
            connection_socket.close()


# The server will receive file, process file, and send file to client
def word_process(connection_socket, user_info):
    """
    The server will receive file,
    process file, and
    send file to client
    """
    # We will receive the data and decode to string
    content_str = ""
    data = connection_socket.recv(1024)
    content_str = data.decode()

    if content_str != "":
        # The server indicate that the file is received
        receive_file_status_label.config(text="The server received the file. ")
        # The server will send the status info to the client
        connection_socket.send("The server received the file. We will get started processing. ".encode())

    # The description says that receive a user-supplied text file from the client.
    # It does not say that the server will store the data to the txt file on the server.
    # So instead of receiving data, creating text file, and storing it the server,
    # we will just receive the data and convert it to the string.
    # So we will not check if there exist the text file in the server and
    # check if the text file is empty or not. We will just check if the string is empty or not.
    # We will use the above code and I prefer to keep the following code here.

    # # https://www.reddit.com/r/learnpython/comments/6z2x97/socket_python_3_file_transfer_over_tcp/
    # # What I have learned
    # # It is the first way that I learned regarding how to receive file
    # # get file name to download
    # f_received = open("received_from_" + user_info + ".txt", "wb")
    # while True:
    #     # get file bytes
    #     # The client and server will complete the handshaking,
    #     # creating a TCP connection between the client’s client_socket and
    #     # the server’s connection_socket
    #     # If we have the TCP connection, we will receive data from client
    #     data = connection_socket.recv(1024)
    #     if not data:
    #         break
    #     # write bytes on file
    #     f_received.write(data)
    # f_received.close()
    # # We will check if the file is received in the server
    # # We create the file on the server, receive the data from client, and save the data to the file
    # # Therefore, the file exist on the server does not necessarily mean that the data is received
    # # The first thing is to check if there exist the text file in the server
    # # The second thing is to check if the text file is empty or not
    # # This code was taken from the website
    # # Source code website:
    # # https://www.guru99.com/python-check-if-file-exists.html
    # # What I have learned
    # # I learned how to check if the file exist
    # if os.path.exists("received_from_" + user_info + ".txt"):
    #     # This code was taken from the website
    #     # Source code website:
    #     # https://stackoverflow.com/questions/2507808/how-to-check-whether-a-file-is-empty-or-not
    #     # Source code:
    #     # import os
    #     # print(os.stat("file").st_size > 0)
    #     # print(os.path.getsize("file") > 0)
    #     # What I have learned
    #     # I learned how to check if the file is empty or not
    #     if os.stat("received_from_" + user_info + ".txt").st_size > 0:
    #         # The server indicate that the file is received
    #         receive_file_status_label.config(text="The server received the file. ")
    #         # The server will send the status info to the client
    #         connection_socket.send("The server received the file. We will get started processing. ".encode())

    # Store the word to content_list
    content_list = content_str.split()

    # Word checking
    # Convert lexicon.txt to string
    f_lexicon = open("lexicon.txt", "r")
    lexicon_str = str(f_lexicon.read())
    f_lexicon.close()
    # Convert string to list
    lexicon_list = lexicon_str.split()
    # Convert list with strings to lowercase
    # This code was taken from the website
    # Source code website:
    # https://stackoverflow.com/questions/1801668/convert-a-python-list-with-strings-all-to-lowercase-or-uppercase
    # What I have learned
    # How to convert list with strings to lowercase
    lexicon_list_lowercase = [x.lower() for x in lexicon_list]
    # Add [] to the words that appear in the lexicon
    for index in range(0, len(content_list)):
        if content_list[index].lower() in lexicon_list_lowercase:
            content_list[index] = "[" + str(content_list[index]) + "]"

    # Store the modified content to modified_content_send_from_server.txt
    modified_content_str = " ".join(content_list)
    # This code was taken from the website
    # Source code website:
    # https://stackoverflow.com/questions/5214578/print-string-to-text-file
    # What I have learned
    # How to print string to text file
    # If you use a context manager, the file is closed automatically for you
    with open("modified_content_send_from_server.txt", "w") as f_modified:
        f_modified.write(modified_content_str)

    # The server indicate that we will send the modified file to the client
    send_file_status_label.config(text="The file process is completed. "
                                       "We are sending file to client. ")
    # We will transfer the modified file to the client
    # This code was taken from the website
    # Source code website:
    # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server
    # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server/blob/master/server.py
    # What I have learned
    # It is the third way that I learned regarding how to send file
    file_name = "modified_content_send_from_server.txt"
    # open file
    with open(file_name, "rb") as f:
        # send file
        data = f.read()
        connection_socket.sendall(data)

    # if file_name != '':
    #     file = open(file_name, 'rb')
    #     data = file.read(1024)
    #     while data:
    #         connection_socket.send(data)
    #         data = file.read(1024)

    # We will close the socket
    connection_socket.close()


# The server should be able to handle all three clients simultaneously
# There is three thread
# This code was taken from the website
# Source code website:
# https://codezup.com/socket-server-with-multiple-clients-model-multithreading-python/
# What I have learned
# I learned that how to create a Multithreading Server
# that can keep track of the threads and the clients which connect to it
start_new_thread(user_check, ())
start_new_thread(user_check, ())
start_new_thread(user_check, ())

# We will create the GUI
root = tkinter.Tk()

# The client GUI title is Word Checking Client
root.title('Word Checking Server')
# The client GUI size is 500 x 500
root.geometry("500x500")

# We will let the user know that the server is running
# Create the ip label
server_ip__label = tkinter.Label(root, text="The server is running on localhost")
server_ip__label.pack(fill=tkinter.X)
# Create the port label
server_port_label = tkinter.Label(root, text="The server is running on port " + str(server_port))
server_port_label.pack(fill=tkinter.X)

# Create the user label
user_label = tkinter.Label(root, text="")
user_label.pack(fill=tkinter.X)
# Create the user list label
user_list_label = tkinter.Label(root, text="")
user_list_label.pack(fill=tkinter.X)

# Create the receive_file_status_label
receive_file_status_label = tkinter.Label(root, text="")
receive_file_status_label.pack(fill=tkinter.X)
# Create the send_file_status_label
send_file_status_label = tkinter.Label(root, text="")
send_file_status_label.pack(fill=tkinter.X)

# Create the quit button
# This code was taken from the website
# Source code website:
# https://www.delftstack.com/howto/python-tkinter/how-to-close-a-tkinter-window-with-a-button/
# What I have learned
# How to close the GUI with the button
quit_client = tkinter.Button(root, text="Quit the server", command=root.quit)
quit_client.pack(fill=tkinter.X)

root.mainloop()
