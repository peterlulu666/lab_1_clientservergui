# We will import the socket module
import socket

import threading
from _thread import *

# It is the same port number as in the client
server_port = 13000
# We will creates the server’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# We will bind the port number 13000 to the server’s socket
server_socket.bind(("", server_port))
# The server_socket is the welcoming socket
# After establishing this welcoming door,
# we will wait and listen for some client to knock on the door
# The server will listen for TCP connection requests from the client
server_socket.listen(3)
# We will let the user know that the server is running
print("The server is running ")


def word_processing(connection_socket, user_info):
    while True:
        # We will receive the data and decode to string
        data = connection_socket.recv(1024)
        content_str = data.decode()

        # Store the word to content_list
        content_list = content_str.split()

        # Word checking
        # Convert lexicon.txt to string
        f_lexicon = open("lexicon.txt", "r")
        lexicon_str = str(f_lexicon.read())
        f_lexicon.close()
        # Convert string to list
        lexicon_list = lexicon_str.split()
        # Convert list with strings to lowercase
        # This code was taken from the website
        # Source code website:
        # https://stackoverflow.com/questions/1801668/convert-a-python-list-with-strings-all-to-lowercase-or-uppercase
        # What I have learned
        # How to convert list with strings to lowercase
        lexicon_list_lowercase = [x.lower() for x in lexicon_list]
        # Add [] to the words that appear in the lexicon
        for index in range(0, len(content_list)):
            if content_list[index].lower() in lexicon_list_lowercase:
                content_list[index] = "[" + str(content_list[index]) + "]"

        # Store the modified content to modified_content_send_from_server.txt
        modified_content_str = " ".join(content_list)
        # This code was taken from the website
        # Source code website:
        # https://stackoverflow.com/questions/5214578/print-string-to-text-file
        # What I have learned
        # How to print string to text file
        # If you use a context manager, the file is closed automatically for you
        with open("modified_content_send_from_server.txt", "w") as f_modified:
            f_modified.write(modified_content_str)

        # How to send file to client
        # get file name to send
        f_send = "modified_content_send_from_server.txt"
        # open file
        with open(f_send, "rb") as f:
            # send file
            data = f.read()
            connection_socket.sendall(data)

        # We will closes the socket
        connection_socket.close()


user_info_list = []


def user_check():
    while True:
        # When a client knocks on this door,
        # it will use the accept() to create a new socket in the server
        connection_socket, address = server_socket.accept()
        user_info = connection_socket.recv(1024).decode()
        if user_info not in user_info_list:
            connection_socket.send("not duplicated".encode())
            user_info_list.append(user_info)
            # https://codezup.com/socket-server-with-multiple-clients-model-multithreading-python/
            # We will create the thread
            start_new_thread(word_processing, (connection_socket, user_info,))
        else:
            connection_socket.send("duplicated".encode())
            connection_socket.close()


user_check()
