# We will import the socket module
import socket

# We will set the server name to localhost
import sys

server_name = "localhost"
# We will arbitrarily choose 13000 for the server port number
server_port = 13000
# We will creates the client’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# The TCP connection must first be established between the client and server
# So we will perform the three way handshake and
# initiate the TCP connection between the client and server
client_socket.connect((server_name, server_port))

user_info = str(input("Enter user info "))
client_socket.send(user_info.encode())

if client_socket.recv(1024).decode() == "duplicated":
    print("The user is duplicated. ")
    sys.exit(0)
else:
    # https://www.reddit.com/r/learnpython/comments/6z2x97/socket_python_3_file_transfer_over_tcp/
    # get file name to send
    f_send = str(input("What file you want to transfer? "))
    # open file
    with open(f_send, "rb") as f:
        # send file
        data = f.read()
        client_socket.sendall(data)

    # How to receive file from server
    # The client will wait the modified_content from server
    # get file name to download
    f = open("modified_file_arrive_at_client.txt", "wb")
    while True:
        # get file bytes
        data = client_socket.recv(1024)
        if not data:
            break
        # write bytes on file
        f.write(data)
    f.close()

    # We will closes the socket
    client_socket.close()
