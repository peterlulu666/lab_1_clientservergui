# We will import the socket module
import socket

# It is the same port number as in the client
server_port = 13000
# We will creates the server’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# We will bind the port number 13000 to the server’s socket
server_socket.bind(("", server_port))
# The server_socket is the welcoming socket
# After establishing this welcoming door,
# we will wait and listen for some client to knock on the door
# The server will listen for TCP connection requests from the client
server_socket.listen(1)
# We will let the user know that the server is running
print("The server is running ")
while True:
    # When a client knocks on this door,
    # it will use the accept() to create a new socket in the server
    connection_socket, address = server_socket.accept()

    # https://www.reddit.com/r/learnpython/comments/6z2x97/socket_python_3_file_transfer_over_tcp/
    # get file name to download
    f = open("received.txt", "wb")

    # get file bytes
    # The client and server will complete the handshaking,
    # create a TCP connection between the client’s client_socket and
    # the server’s connection_socket
    # If we have the TCP connection, we will receive data from client
    while True:
        # get file bytes
        connection_socket.setblocking(0)
        try:
            data = connection_socket.recv(1024)
        except BlockingIOError:
            data = 0
        if not data:
            break
        # write bytes on file
        f.write(data)
    f.close()

    # How to send file to client
    # get file name to send
    f_send = "received.txt"

    # open file
    with open(f_send, "rb") as f:
        # send file
        data = f.read()
        connection_socket.sendall(data)

    # We will closes the socket
    connection_socket.close()
